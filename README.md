Scala Tail Recursion Examples
-----------------------------


# compile the project

sbt compile


# Run either SumIt (adds numbers from 1 to the input) or Fib (finds fibonacci number at sequence n) using the given strategy.

sbt "run-main [SumIt|Fib] 10 [iterative|recursive|tail]"