object Helpers {
  def TimeIt(f: (Int) => BigInt, num: Int) = {
    val t0 = System.nanoTime
    val result = f(num)
    val t1 = System.nanoTime
    (result.toString, (t1 - t0) * 0.000001)
  }
}