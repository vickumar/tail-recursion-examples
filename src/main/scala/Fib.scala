import scala.annotation.tailrec

object Fib {

    private def Fib (x: Int): BigInt = {
        var n1: BigInt = 1
        var n2: BigInt = 1
        var fib: BigInt = 1

        if (x <= 1)
            1
        else {
            var i = 0
            for (i <- 2 to x) {
                n1 = n2
                n2 = fib
                fib = n1 + n2
            }
            fib
        }
    }

    private def Fib2 (x: Int): BigInt = {
        if (x < 2)
            1
        else
            Fib2(x - 2) + Fib2(x - 1)
    }

    private def Fib3 (x: Int): BigInt = {
        if (x < 2)
            1
        else
            FibTail(x, 1, 2)
    }


    @tailrec
    private def FibTail (x: Int, a: BigInt = 0, b: BigInt = 0): BigInt = {
        if (x <= 1)
            a
        else
            FibTail(x - 1, b, a + b)
    }

    def main(args: Array[String]) {
        val num = args(0).toInt

        val (sum, tot) = args(1).toString match {
            case "iterative" => Helpers.TimeIt(Fib, num)
            case "recursive" => Helpers.TimeIt(Fib2, num)
            case _ => Helpers.TimeIt(Fib3, num)
        }

        println("result: %s, time: %s ms".format(sum, tot))
    }
}
