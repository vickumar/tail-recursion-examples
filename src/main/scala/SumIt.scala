import scala.annotation.tailrec

object SumIt {

    private def AddNums (x: Int): BigInt = {
        var total = 0
        var i = 0
        for (i <- 1 to x)
        {
            total += i
        }
        total
    }

    private def AddNums2 (x: Int): BigInt = {
        if (x <= 1)
            1
        else
            x + AddNums2(x - 1)
    }

    private def AddNums3 (x: Int) = AddNumsTail(x, 0)

    @tailrec
    private def AddNumsTail (x: Int, currSum: BigInt = 0): BigInt = {
        if (x <= 0)
            currSum
        else
            AddNumsTail(x - 1, x + currSum)
    }

    def main(args: Array[String]) {
        val num = args(0).toInt

        val (sum, tot) = args(1).toString match {
            case "iterative" => Helpers.TimeIt(AddNums, num)
            case "recursive" => Helpers.TimeIt(AddNums2, num)
            case _ => Helpers.TimeIt(AddNums3, num)
        }

        println("result: %s, time: %s ms".format(sum, tot))
    }
}



